package net.picrypto.user.domain;

import java.util.Objects;

public class Reward {

    private long rewardId;
    private String surveyId;
    private  String userId;
    private double rewardPoints;

    public Reward(long rewardId, String surveyId, String userId, double rewardPoints) {
        this.rewardId = rewardId;
        this.surveyId = surveyId;
        this.userId = userId;
        this.rewardPoints = rewardPoints;
    }

    public Reward() {
    }

    public long getRewardId() {
        return rewardId;
    }

    public void setRewardId(long rewardId) {
        this.rewardId = rewardId;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getRewardPoints() {
        return rewardPoints;
    }

    public void setRewardPoints(double rewardPoints) {
        this.rewardPoints = rewardPoints;
    }

    @Override
    public String toString() {
        return "Reward{" +
                "rewardId=" + rewardId +
                ", surveyId='" + surveyId + '\'' +
                ", userId='" + userId + '\'' +
                ", rewardPoints=" + rewardPoints +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reward reward = (Reward) o;
        return rewardId == reward.rewardId && Double.compare(reward.rewardPoints, rewardPoints) == 0 && surveyId.equals(reward.surveyId) && userId.equals(reward.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rewardId, surveyId, userId, rewardPoints);
    }
}

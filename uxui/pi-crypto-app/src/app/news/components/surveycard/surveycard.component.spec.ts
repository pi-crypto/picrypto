import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveycardComponent } from './surveycard.component';

describe('SurveycardComponent', () => {
  let component: SurveycardComponent;
  let fixture: ComponentFixture<SurveycardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurveycardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveycardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

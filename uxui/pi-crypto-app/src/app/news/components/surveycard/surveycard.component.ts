import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';
import { News } from '../../news';
import { NewsService } from '../../news.service';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-surveycard',
  templateUrl: './surveycard.component.html',
  styleUrls: ['./surveycard.component.css']
})
export class SurveycardComponent implements OnInit {
news:News;
 description:string;
 title:string;
 content:string;
 actionType:string
 author:string
 source:string

  constructor(public dialogRef:MatDialogRef<SurveycardComponent>,@Inject(MAT_DIALOG_DATA) public data: any, private newsservice:NewsService) { 
    this.description=data.obj.description;
    this.title=data.obj.title;
    this.content=data.obj.content;
    this.news=data.obj;
    this.author=this.news.author;
    this.source=this.news.source_name;
    this.actionType=data.actionType;

  }

  ngOnInit(): void {
  }
  onNoClick(){
    this.dialogRef.close();
  }
  submitSurvey(){
    //process answers and show pi point card.
    //call newsservice submittNewsSurvey
  }
}

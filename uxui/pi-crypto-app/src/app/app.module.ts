import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatCardModule} from '@angular/material/card';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import{RouterModule,Routes} from '@angular/router';
import {NewsModule} from './news/news.module';
import{MatToolbarModule} from '@angular/material/toolbar';
import{MatDialogModule} from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';

const appRoutes: Routes =[
  {
    path:'',
  	redirectTo:'/news',
  	pathMatch:'full'
  }
  
 
]

@NgModule({
  declarations: [
    AppComponent,
    
 
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    NewsModule,
    MatToolbarModule,
    MatInputModule,
    MatDialogModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

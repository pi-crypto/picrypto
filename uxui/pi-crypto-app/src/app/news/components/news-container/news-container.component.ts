import { ActivatedRoute } from '@angular/router';
import { NewsService } from './../../news.service';
import { Component, OnInit } from '@angular/core';
import { News } from '../../news';
@Component({
  selector: 'app-news-container',
  templateUrl: './news-container.component.html',
  styleUrls: ['./news-container.component.css']
})
export class NewsContainerComponent implements OnInit {
newsArticles: Array<News>;
  newsType: string;
  constructor(private newsService : NewsService,private route:ActivatedRoute) {
    this.newsArticles=[];
    this.route.data.subscribe((data)=>{this.newsType=data.newsType});
   }

  ngOnInit() {
    this.newsService.getNews(this.newsType).subscribe((newsArticles)=>{this.newsArticles.push(...newsArticles);});
  }

}

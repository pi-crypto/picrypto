import { NewsService } from './../../news.service';
import { Component, Input, OnInit,Output } from '@angular/core';
import { News } from '../../news';
import { EventEmitter } from '@angular/core';
import {MatDialog,MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog';
import { SurveycardComponent } from '../surveycard/surveycard.component';
@Component({
  selector: 'app-newscard',
  templateUrl: './newscard.component.html',
  styleUrls: ['./newscard.component.css']
})
export class NewscardComponent implements OnInit {
  @Input()
  news: News;
  @Output()
  addNews=new EventEmitter();
  constructor(private newsService : NewsService,public dialog:MatDialog) { }

  ngOnInit() {
  }
  addToNewsItem() {
    
    console.log()
    this.addNews.emit(this.news);

 
  }
  submitSurvey(actionType)
{

         let dialogRef= this.dialog.open(SurveycardComponent,{
         width:'500px',
         data:{obj:this.news,actionType:actionType}
       });
    
    dialogRef.afterClosed().subscribe(result=>{console.log('The dialog was closed');
  });
}
}

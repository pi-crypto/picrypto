export interface News {
    id: number,
    source_id:string,
    source_name:string,
    author: string,
    title: string,
    description:string,
    urlToImage: string,
    url:string,
    published_at:string,
    content:string,
    user_id:string

}


package net.picrypto.user.domain;

import java.util.Date;
import java.util.Objects;

public class Comment{

    private String commentId;
    private String commentText;
    private Date createdDate;
    private Date modifiedDate;
    private String userId;

    public Comment(String commentId, String commentText, Date createdDate, Date modifiedDate, String userId) {
        this.commentId = commentId;
        this.commentText = commentText;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.userId = userId;
    }

    public Comment() {
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commentId='" + commentId + '\'' +
                ", commentText='" + commentText + '\'' +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                ", userId='" + userId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return commentId.equals(comment.commentId) && Objects.equals(commentText, comment.commentText) && createdDate.equals(comment.createdDate) && modifiedDate.equals(comment.modifiedDate) && userId.equals(comment.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commentId, commentText, createdDate, modifiedDate, userId);
    }
}

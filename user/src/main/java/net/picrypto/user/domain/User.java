package net.picrypto.user.domain;

import java.util.Objects;

public class User {

    private String userId;
    private String password;
    private String emailId;
    private String firstName;
    private String lastName;
    private String username;
    //non redeemable;
    private double piPoints;
    //redeemable;
    private double piPoints2;

    public User(String userId, String password, String emailId, String firstName, String lastName, String username, long piPoints, long piPoints2) {
        this.userId = userId;
        this.password = password;
        this.emailId = emailId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.piPoints = piPoints;
        this.piPoints2 = piPoints2;
    }

    public User() {
    }

    //getter and setters

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getPiPoints() {
        return piPoints;
    }

    public void setPiPoints(double piPoints) {
        this.piPoints = piPoints;
    }

    public double getPiPoints2() {
        return piPoints2;
    }

    public void setPiPoints2(double piPoints2) {
        this.piPoints2 = piPoints2;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return piPoints == user.piPoints && piPoints2 == user.piPoints2 && userId.equals(user.userId) && password.equals(user.password) && emailId.equals(user.emailId) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, password, emailId, firstName, lastName, username, piPoints, piPoints2);
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", password='" + password + '\'' +
                ", emailId='" + emailId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", piPoints=" + piPoints +
                ", piPoints2=" + piPoints2 +
                '}';
    }
}

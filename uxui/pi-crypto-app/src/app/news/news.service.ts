import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators/map';
import { Observable } from 'rxjs/Observable';
import { News} from './news';
import {retry} from 'rxjs/operators';

@Injectable()
export class NewsService{
  topNewsEndPoint: string;
  apiKey:string;
  searchNewsEndpoint: string;
  constructor(private http:HttpClient) { 
  this.apiKey='apiKey=ddb07f97f97d4bb089971309bf5551a2';
  this.topNewsEndPoint ='https://newsapi.org/v2/top-headlines?country=in&category=business&'+this.apiKey+'&page=1';
  this.searchNewsEndpoint='https://newsapi.org/v2/everything?q=';
}

getNews(type: string, page: number =1 ): Observable<Array<News>>  {
  const endpoint = `${this.topNewsEndPoint}`;
  return this.http.get(endpoint).pipe(
    retry(3),
     map(this.pickNewsResults),
     map(this.transformPosterPath.bind(this)),
     map(this.mapSource.bind(this)));
}
mapSource(newsArticles): Array<News>{
  return newsArticles.map(news => {
   
    news.source_id=`${news.source.id}`;
    news.source_name=`${news.source.name}`;
    return news;
  });
}
      transformPosterPath(newsArticles): Array<News>{
        return newsArticles.map(news => {
          news.urlToImage= `${news.urlToImage}`;
           return news;
        });
      }
     pickNewsResults(response){
       return response['articles'];
     }
     
   
   submitNewsSurvey()
   {
      //newsService call to submit survey
   }
    
}
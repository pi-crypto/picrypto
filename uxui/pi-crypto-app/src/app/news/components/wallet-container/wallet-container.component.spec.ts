import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletContainerComponent } from './wallet-container.component';

describe('WalletContainerComponent', () => {
  let component: WalletContainerComponent;
  let fixture: ComponentFixture<WalletContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WalletContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

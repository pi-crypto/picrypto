import { WalletCardComponent } from './components/wallet-card/wallet-card.component';
import { SurveycardComponent } from './components/surveycard/surveycard.component';
import { NewsService } from './news.service';
import { MatCardModule } from '@angular/material/card';
import { NewsRouterModule } from './news-router.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerComponent } from './components/container/container.component';
import { NewsContainerComponent } from './components/news-container/news-container.component';
import { NewscardComponent } from './components/newscard/newscard.component';
import { WalletContainerComponent } from './components/wallet-container/wallet-container.component';
import { CryptoContainerComponent } from './components/crypto-container/crypto-container.component'
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatDividerModule} from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import{MatToolbarModule} from '@angular/material/toolbar';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { CryptoTrendCardComponent } from './components/crypto-trend-card/crypto-trend-card.component';
@NgModule({
 
  imports: [
    CommonModule,
    HttpClientModule,
    NewsRouterModule,
    MatCardModule,
    MatButtonModule,
    FormsModule,
    MatInputModule,
    MatDividerModule,
    MatToolbarModule,
    MatDialogModule,
    MatInputModule
  ],
  declarations: [
    ContainerComponent,
    NewsContainerComponent,
    NewscardComponent,
    WalletContainerComponent,
    CryptoContainerComponent,
    SurveycardComponent,
    TopBarComponent,
    WalletCardComponent,
    CryptoTrendCardComponent
  ],
  entryComponents:[SurveycardComponent],
  exports:[
    NewsRouterModule,
    ContainerComponent,
    NewsContainerComponent,
    NewscardComponent,
    WalletContainerComponent,
    CryptoContainerComponent,
    SurveycardComponent,
    TopBarComponent,
    WalletCardComponent,
    CryptoTrendCardComponent
  ],
  providers: [NewsService],
})
export class NewsModule { }

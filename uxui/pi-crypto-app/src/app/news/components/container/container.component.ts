import { NewsService } from './../../news.service';
import { Component, Input, OnInit } from '@angular/core';
import { News } from '../../news';

import { NewscardComponent } from '../newscard/newscard.component';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {
  @Input()
  newsArticles: Array<News>;

  constructor(private newsService : NewsService) { }

  ngOnInit(): void {
  }

}

# PiCrypto App

**What is it?**

PiCrypto App is a reward based social networking news App on top of the Pi Ecosystem with a goal to make the crypto-unaware society crypto-aware by making them the daily cryptocurrency related news around the world available at their finger-tips. 

Rewarding mechanism: This app rewards the users with Real Pi coins for reading the news (by filling a news related short-survey) and for appreciating the right content created regularly based on a properly thought-out Algorithm.

How does PiCrypto distribute rewards among its users?

There are 3 wallets involved in the interaction:

  1) The Mining Pi wallet includes the mined/purchased Pi of the Pioneers.

  2) The Gifting wallet includes Pi-points used for gifting to the content providers and Pioneers will receive 10 free Pi-points to gift to the content creators every month & these will not be transferable to the gifting Pioneer’s Mining Pi Wallet and will expire if not used by the end of the month. 

  3) Pioneer's Reward collection wallet – which collects the reward Pi received after filling and submitting the fact-check surveys about the news read by the Pioneer, and these Pi will be transferable to the pioneer's Mining Pi Wallets. The rewarding algorithm rewards regular news reader Pioneers of the PiCrypto App more often and it also takes into consideration whether the pioneers use up their free Pi-points regularly to reward the right content creators.

The news fact check surveys are in development phase at the moment and we are planning to complete it before the Hackathon deadline.

 **The Development plan**


<details><summary><strong>Phase 1: The news, The Surveys and The Rewards</strong></summary>

- Currently in development for Pi's first hackathon.
- We are going to have a platform where a Pioneer can access crypto related news in one place.
- A pioneer can read this news, answer a survey related to news and win a reward Pi-points which will be accumulated in Pioneer's Reward Collection Wallet.
- Pioneers can then reedem this Pi-points using pi payments and this will be addded to Pioneer's Mining pi Wallets.
- Timeline - July 15 2021-   July 29 2021

</details>
<details><summary><strong>Phase 2: The Pioneers, The Subscription, The Content creation and Appreciation </strong></summary>

- This is in design stage
 The plan is to introduce subscription plans for pioneers on this platform using Pi's Payment gateway.
-A subscription plan will add limited number of Points in Pioneer's Gifting Wallet.
- The Pioneer will appreciate a Content created by other Pioneer by gifing the content creator with some Pi-Points, in return Pioneer would also recevie some Pi points which will be added to Rewards Collection Wallet.
-Proposed Timeline - August 2021- November 2021
 

</details>
With this App the PiCrypto App team wishes to bring about an awareness about the crypto currency world to the common man.


-PiCrypto Team

(Updated: 19 July, 2021)



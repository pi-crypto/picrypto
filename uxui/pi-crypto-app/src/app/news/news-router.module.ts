import { NgModule, Component } from '@angular/core';
import{RouterModule, Routes} from '@angular/router';
import { NewsContainerComponent } from './components/news-container/news-container.component';

const newsRoutes: Routes =[
{
  path:'news',
  children:[
    {
      path:'',
      redirectTo:'/news/trending',
      pathMatch:'full'
    },
    {
      path:'trending',
      component: NewsContainerComponent,
      data:{
        newsType: 'trending'
      },
    },
    {
      path:'dashboard',
      redirectTo:'news/dashboard',
      pathMatch:'full'
    },
    
  ]
}
];
@NgModule({
  imports:[RouterModule.forChild(newsRoutes)],
  exports:[RouterModule]
})
export class NewsRouterModule{

}
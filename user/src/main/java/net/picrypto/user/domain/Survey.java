package net.picrypto.user.domain;

import java.util.Objects;

public class Survey {

    private String userId;
    private String newsId;
    private String surveyId;
    private String surveyQuery;
    private String surveyResponseFromUser;
    private long rewardId;

    public Survey(String userId, String newsId, String surveyId, String surveyQuery, String surveyResponseFromUser, long rewardId) {
        this.userId = userId;
        this.newsId = newsId;
        this.surveyId = surveyId;
        this.surveyQuery = surveyQuery;
        this.surveyResponseFromUser = surveyResponseFromUser;
        this.rewardId = rewardId;
    }

    public Survey() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getSurveyQuery() {
        return surveyQuery;
    }

    public void setSurveyQuery(String surveyQuery) {
        this.surveyQuery = surveyQuery;
    }

    public String getSurveyResponseFromUser() {
        return surveyResponseFromUser;
    }

    public void setSurveyResponseFromUser(String surveyResponseFromUser) {
        this.surveyResponseFromUser = surveyResponseFromUser;
    }

    public long getRewardId() {
        return rewardId;
    }

    public void setRewardId(long rewardId) {
        this.rewardId = rewardId;
    }

    @Override
    public String toString() {
        return "Survey{" +
                "userId='" + userId + '\'' +
                ", newsId='" + newsId + '\'' +
                ", surveyId='" + surveyId + '\'' +
                ", surveyQuery='" + surveyQuery + '\'' +
                ", surveyResponseFromUser='" + surveyResponseFromUser + '\'' +
                ", rewardId=" + rewardId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Survey survey = (Survey) o;
        return rewardId == survey.rewardId && userId.equals(survey.userId) && newsId.equals(survey.newsId) && surveyId.equals(survey.surveyId) && surveyQuery.equals(survey.surveyQuery) && Objects.equals(surveyResponseFromUser, survey.surveyResponseFromUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, newsId, surveyId, surveyQuery, surveyResponseFromUser, rewardId);
    }
}

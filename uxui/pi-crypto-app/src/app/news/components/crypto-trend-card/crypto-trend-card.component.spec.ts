import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CryptoTrendCardComponent } from './crypto-trend-card.component';

describe('CryptoTrendCardComponent', () => {
  let component: CryptoTrendCardComponent;
  let fixture: ComponentFixture<CryptoTrendCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CryptoTrendCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CryptoTrendCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
